import java.awt.*;

public class Cell extends Rectangle {

    int ter;

    public Cell(int x, int y) {
        super(x, y, 35, 35);
    }

    public void paint(Graphics g, Boolean highlighted) {
        if (highlighted) {
            g.fillRect(x+3, y+3, 35-5, 35-5);
        } else {
            g.fillRect(x+1, y+1, 35-1, 35-1);
        }

        g.setColor(Color.BLACK);
        g.drawRect(x, y, 35, 35);
    }

    @Override
    public boolean contains(Point target){
        if (target == null)
            return false;
        return super.contains(target);
    }
}
