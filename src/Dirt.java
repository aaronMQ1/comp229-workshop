import java.awt.*;

public class Dirt extends Cell {

        public Dirt(int x, int y){

            super(x,y);

            ter = 3;

        }

    @Override
    public void paint(Graphics g, Boolean highlighted){

        Color dirtBrown = new Color(139,69,19);
        g.setColor(dirtBrown);

        super.paint(g, highlighted);



    }
}
