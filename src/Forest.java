import java.awt.*;

public class Forest extends Cell {

    public Forest(int x, int y){

        super(x,y);

        ter = 0;

    }

    @Override
    public void paint(Graphics g, Boolean highlighted){

        Color forestGreen = new Color(34,139,34);
        g.setColor(forestGreen);

        super.paint(g, highlighted);



    }

}
