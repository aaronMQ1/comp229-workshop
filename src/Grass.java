import java.awt.*;

public class Grass extends Cell {
    public Grass(int x, int y){

        super(x,y);

        ter = 1;

    }

    @Override
    public void paint(Graphics g, Boolean highlighted){

        Color grassGreen = new Color(50,205,50);
        g.setColor(grassGreen);

        super.paint(g, highlighted);



    }
}
