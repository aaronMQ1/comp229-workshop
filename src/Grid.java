import bos.GameBoard;
import bos.GamePiece;

import java.awt.*;
import java.util.Optional;
import java.util.Random;

public class Grid implements GameBoard<Character>{

    private Cell[][] cells  = new Cell[20][20];

    private int x;
    private int y;
    int clumping = 120000;

    public Grid(int x, int y) {
        this.x = x;
        this.y = y;

        for(int i = 0; i < 20; i++) {
            for(int j = 0; j < 20; j++) {

                int terrain = genTerrain(i, j);

                switch (terrain){


                    case 0:
                        //fores:dark green
                        cells[i][j] = new Forest(x + j * 35, y + i * 35);
                        break;

                    case 1:
                        //grass: light green
                        cells[i][j] = new Grass(x + j * 35, y + i * 35);
                        break;

                    case 2:
                        //Stone: grey
                        cells[i][j] = new Stone(x + j * 35, y + i * 35);
                        break;

                    case 3:
                        //dirt: brown
                        cells[i][j] = new Dirt(x + j * 35, y + i * 35);
                        break;
                }

            }
        }
    }

    public int genTerrain(int row, int col){

        if(clumping < 40){
            clumping = 12000;
        }

        Random terRandom = new Random();
        int coin = terRandom.nextInt(clumping);

        int terrain = 0;




        if(row > 0 && coin > 10 && coin < ((clumping/2) + 5)){
            terrain = cells[row - 1][col].ter;
            if(clumping > 20) clumping /= 2;


        }
        else if( col > 0 && coin > ((clumping/2) + 5)){
            terrain = cells[row ][ col- 1].ter;
            if(clumping > 20) clumping /= 2;


        }
        else{
            terrain = terRandom.nextInt(4);
            // clumping = 1200;
        }


        return terrain;

    }

    public void paint(Graphics g, Point mousePosition) {
        for(int y = 0; y < 20; ++y) {
            for(int x = 0; x < 20; ++x) {
                Cell thisCell = cells[x][y];
                thisCell.paint(g, thisCell.contains(mousePosition));
            }
        }
    }

    public Cell getRandomCell(){
        java.util.Random rand = new java.util.Random();
        return cells[rand.nextInt(20)][rand.nextInt(20)];
    }


    @Override
    public Optional<Character> below(Character character) {
        return null;
    }

    @Override
    public Optional<Character> above(Character character) {
        return null;
    }

    @Override
    public Optional<Character> rightOf(Character character) {
        return null;
    }

    @Override
    public Optional<Character> leftOf(Character character) {
        return null;
    }
}
