import bos.MoveDown;
import bos.RelativeMove;

import java.awt.*;
import java.time.*;
import java.util.List;

public class Stage {
    private Grid grid;
    private Character sheep;
    private Character shepherd;
    private Character wolf;
    private List<bos.RelativeMove> moves;
    private Instant timeOfLastMove;

    public Stage() {
        grid     = new Grid(10, 10);
        sheep    = new Sheep(grid.getRandomCell());
        shepherd = new Shepherd(grid.getRandomCell());
        wolf     = new Wolf(grid.getRandomCell());
/*
        RelativeMove Down = new MoveDown(grid, sheep);
        moves.add(Down);*/
    }


    public void update() {

        if (moves.size() > 0 && timeOfLastMove.plus(Duration.ofSeconds(2)).isBefore(Instant.now())) {
            timeOfLastMove = Instant.now();
            moves.remove(0).perform();
        } else if (moves.size() == 0 && timeOfLastMove.plus(Duration.ofSeconds(20)).isBefore(Instant.now())) {
            System.exit(0);
        }
    }

    public void paint(Graphics g, Point mouseLocation) {
        grid.paint(g, mouseLocation);
        sheep.paint(g);
        shepherd.paint(g);
        wolf.paint(g);
    }
}