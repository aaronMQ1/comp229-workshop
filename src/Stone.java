import java.awt.*;

public class Stone extends Cell {
        public Stone(int x, int y){

            super(x,y);

            ter = 2;

        }

    @Override
    public void paint(Graphics g, Boolean highlighted){

        Color stoneGrey = new Color(119,136,153);
        g.setColor(stoneGrey);

        super.paint(g, highlighted);



    }
}
